<?php

namespace App;

//use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Role;

 
class User extends Authenticatable
{
//     //use Notifiable;
//
//     public $table='users';
//    /**     //use Notifiable;
//
//     * The attributes that are mass assignable.
//     *
//     * @var array
//     */
//    protected $fillable = [
//        'name', 'email', 'password',
//    ];
//
//    /**
//     * The attributes that should be hidden for arrays.
//     *
//     * @var array
//     */
//    protected $hidden = [
//        'password', 'remember_token',
//    ];
//    
//    public function roles()
//    {
//       //return $this->belongsToMany('App\Role');
//        return $this->belongsToMany('App\Role', 'roles', 'id', 'role_id');
//    }
//    
//    public function isUser()
//    {
//        return ($this->roles()->count()) ? true : false;
//    }
//    
//    public function hasRole($role)
//    {
//        return in_array($role, $this->roles->pluck('name'));
//    }
//    
//    private function getIdArray($array, $term)
//    {
//        foreach ($array as $key => $value) {
//            if($value == $term)
//            {
//                return $key;
//            }
//        }
//        
//        throw new UnexpectedValueExceotion;
//    }
//    
//        public function is($roleName)
//    {
//        foreach ($this->roles()->get() as $role)
//        {
//            if ($role->name == $roleName)
//            {
//                return true;
//            }
//        }
//
//        return false;
//    }
//    
//    public function makeUsers($title)
//    {
//        $assigned_roles = array();
//        $roles = Role::all()->pluck('name', 'id');
//        
//        echo $roles;
//      
//        switch ($title)
//        {
//           
//            case 'super_user':
//                $assigned_roles[] = $this->getIdArray($roles, 'user');
//                
//            case 'super_admin':
//               $assigned_roles[] = $this->getIdArray($roles, 'admin');
//                break;
//                
//            default:
//                throw new Exception('The user status does not exists');
//        }
//        
//       $this->roles()->sync($assigned_roles);
//        
//    }
    protected $fillable = ['email', 'password'];
    public $table = 'users';
    
    
    public function roles()
    {
        return $this->belongsToMany('App\Role', 'user_role', 'user_id', 'role_id');
    }
    
    public function hasAnyRole($roles)
    {
        if(is_array($roles)) {
            foreach ($roles as $role)
            {
                if($this->hasRole($role))
                {
                    return true;
                }
            }
        } else {
            if($this->hasRole($roles))
            {
                return true;
            }
        }
        return false;
    }
    
    public function hasRole($role)
    {
        if($this->roles()->where('name', $role)->first()) {
            return true;
        }
        
    }
    
    
}
