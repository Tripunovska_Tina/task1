<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
//    public $timestamps = false;
//    public $table='roles';
//    public function users()
//    {
//      //  return $this->belongsToMany('App\User');
//         $users = DB::table('users')->where('role_id', '1')->get();
//         return $this->belongsToMany('App\User', 'roles', 'role_id', 'id');
//    }
    
    public function users()
    {
        return $this->belongsToMany('App\User', 'user_role', 'role_id', 'user_id');
    }
}
