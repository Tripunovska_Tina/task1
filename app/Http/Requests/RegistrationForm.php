<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegistrationForm extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username'=>'required|unique:username|max:50|min:3',
            'email' =>'required|max:60|email',
            'firstname' => 'required|min:3|max:60',
            'lastname' => 'required|min:3|max:60',
            'password' => 'required|min:3|max:60',
        ];
    }
}
