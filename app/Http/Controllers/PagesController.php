<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Controllers;

use Illuminate\Notifications\Notifiable;
//use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Role;

class PagesController extends Controller {
    
    public function getIndex(){
        return view('main');
    }
    
    public function getAbout(){
        return view('pages.about');
    }
    
    public function getContact(){
        return view('pages.contact');
    }
    
    public function getLogin(){
       return view('pages.login');
    }
    
      public function getRegister(){
        return view('pages.register');
    }
    
     public function getEdit(){
        return view('posts.edit');
    }
    
    public function getSearch()
    {
        return view('pages.search');
    }
    
    public function getUser()
    {
        return view('findUser');
    }
    
    public function getUpdateData() {
        return view('updateData.updateData');
    }
    
    
    
}
