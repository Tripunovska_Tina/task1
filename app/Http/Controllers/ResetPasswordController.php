<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\User;

class ResetPasswordController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
     $user = User::find($id);
        return view('userprofile.edit')->withUser($user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //Validate the data
        $user = User::find($id);
        $passInput = $request->input('password');
        $newPass = $request->input('newpassword');
        $confNewPass = $request->input('confPassword');
        $DBPass = $user->password;
        
        //$passInput = $request->input('password');
        
        
//        if($currPass === $DBPass) 
//        {
//            
//            if($newPass === $confNewPass) {
//                 $user->password = bcrypt($newPass);
//                 $user->save();
//                 return redirect()->route('userprofile.create', $user->id);
//            } else {
//                echo 'No matches.';
//            }
//        } else {
//            echo 'Invalid password';
//        }
        
        //Save the data to the database
        
        //redirect with flash data 
        //return redirect()->route('userprofile.create', $user->id);
        
         if($newPass == $confNewPass) {
                $user->password = bcrypt($newPass);
                 $user->save();
                 return redirect()->route('userprofile.create', $user->id);
           } else {
               echo 'No matches.';
           }
        }
        
        
    

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    
     public function changePassword(){
        return view('resetpass.index');
    }
}
