@extends('main')

@section('content')

  <form class="form-horizontal" role="form" method="post" action="{{ route('resetpass.update', Auth::user()->id) }}" >
            {{ csrf_field() }}

          <div class="form-group">
            <label class="col-lg-3 control-label">Password:</label>
            <div class="col-lg-8">
                <input class="form-control" value="" type="password" name="password">
            </div>
          </div>

         <div class="form-group">
            <label class="col-lg-3 control-label">New password:</label>
            <div class="col-lg-8">
                <input class="form-control" value="" type="password" name="newpassword">
            </div>
            
          </div>
        <div class="form-group">
            <label class="col-lg-3 control-label">Confirm your password:</label>
            <div class="col-lg-8">
                <input class="form-control" value="" type="password" name="confPassword">
            </div>
          </div>
            
          <div class="form-group">
            <label class="col-md-3 control-label"></label>
            <div class="col-md-8">
              <input class="btn btn-primary" value="Save changes" type="submit">
              <span></span>
              <input class="btn btn-default" value="Cancel" type="reset">
            </div>
          </div>
        </form>

<hr>

@endsection