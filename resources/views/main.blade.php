<!DOCTYPE>

<html>
    <head>
        
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Task1</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
           <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script> 
    </head>
    
    <body>
        
        
            <nav class="navbar navbar-default">
                <div class="container-fluid">

                    <!-- Logo -->
                    <div class="navbar-header">
                        <a href="" class="navbar-brand">Digital Point</a>
                    </div>

                    <!-- Menu -->
                    <div>
                        <ul class="nav navbar-nav">
                            <li class="active"><a href="">Home</a> </li>
                            <li><a href="about">About</a></li>
                            <li><a href="contact">Contact us</a></li>
                             <li><a href="login">Log in</a></li>
                             <li><a href="search">Search</a></li>
                        </ul>
                    </div>


                </div>
            </nav>
        
         <div class="container"> 
              @yield('content')
        </div>
        
        
        
    </body>
</html>