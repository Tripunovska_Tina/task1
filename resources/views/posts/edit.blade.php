@extends('main')

@section('content')

    <div class="container">
        <h1>Edit Profile</h1>
            <hr>
            <div class="row">   
            {!! Form::model($users, ['route' => ['posts.update', $users->id, 'method' => 'PUT']]) !!}
         

            <!--edit form column -->
           {{ Form::label('email', 'Email:') }}
           {{ Form::text('email', null, array('class' => 'form-control', 'style' => 'margin-bottom: 10px;'))}}
            
           {{ Form::label('password', 'Password:') }}
           {{ Form::password('password', null, array('class' => 'form-control', 'style' => 'margin-bottom: 10px;'))}}
          
           <div class="col-md-9 personal-info">
            
            <h3>Personal info</h3>

            <form class="form-horizontal" role="form">
                
                {{ Form::label('email', 'Email:') }}
                {{ Form::text('email', null, array('class' => 'form-control', 'style' => 'margin-bottom: 10px;'))}}
            
                {{ Form::label('password', 'Password:') }}
                {{ Form::password('password', null, array('class' => 'form-control', 'style' => 'margin-bottom: 10px;'))}}
                
<!--                {{ Form::label('confirm_password', 'Password:') }}
                {{ Form::password('confirm_password', null, array('class' => 'form-control', 'style' => 'margin-bottom: 10px;'))}}-->
                

              <div class="form-group">
                <label class="col-lg-3 control-label">First name:</label>
                <div class="col-lg-8">
                  <input class="form-control" type="text">
                </div>
              </div>
              <div class="form-group">
                <label class="col-lg-3 control-label">Last name:</label>
                <div class="col-lg-8">
                  <input class="form-control" type="text">
                </div>
              </div>
              
              <div class="form-group">
                <label class="col-lg-3 control-label">Email:</label>
                <div class="col-lg-8">
                  <input class="form-control" type="text">
                </div>
              </div>
              
              <div class="form-group">
                <label class="col-md-3 control-label">Username:</label>
                <div class="col-md-8">
                  <input class="form-control" type="text">
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Password:</label>
                <div class="col-md-8">
                  <input class="form-control" type="password">
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Confirm password:</label>
                <div class="col-md-8">
                  <input class="form-control" type="password">
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label"></label>
                <div class="col-md-8">
                  <input class="btn btn-success" value="Save Changes" type="button">
                  <span></span>
                  <input class="btn btn-primary" value="Cancel" type="reset">
                </div>
              </div>
            </form>
          </div>
          
           {!! Form::close() !!}
      </div>
    </div>
    <hr>

@endsection