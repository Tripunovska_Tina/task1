@extends('main')

@section('content')

<div class="row">
    <div class="col-md-4 col-md-offset-2">
        <h1> Login </h1>
        <hr>
        
        {!! Form::open(array('route' => 'posts.store')) !!}
            {{ Form::label('username', 'Username:') }}
            {{ Form::text('username', null, array('class' => 'form-control', 'style' => 'margin-bottom: 10px;'))}}
            
            {{ Form::label('password', 'Password:') }}
            {{ Form::password('password', null, array('class' => 'form-control'))}}
            
            {{Form::submit('Log in', array('class' => 'btn btn-success btn-lg btn-block', 'style'=>'margin-top: 20px; margin-bottom: 40px;'))}}
            
             
            {{ Form::label('noAcc', 'Don\'t have an account?') }}
              
                   
                       <a href="register"> <br>Register
                       {{--Form::submit('Register', array('class' => 'btn btn-success btn-lg btn-block'))--}}
                       </a>
                   
            
              
    
        {!! Form::close() !!}
    </div>
</div>

@endsection