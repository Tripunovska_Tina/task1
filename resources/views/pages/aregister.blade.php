@extends('main')

@section('content')

<div class="row">
    <div class="col-md-4 col-md-offset-2">
        <h1> Register hhy</h1>
        <hr>
        
        {!! Form::open(array('route' => 'posts.store')) !!}
            {{ Form::label('username', 'Username:') }}
            {{ Form::text('username', null, array('class' => 'form-control', 'style' => 'margin-bottom: 10px;'))}}
            
            {{ Form::label('email', 'E-mail:') }}
            {{ Form::text('email', null, array('class' => 'form-control', 'style' => 'margin-bottom: 10px;'))}}
            
            {{ Form::label('firstname', 'First name:') }}
            {{ Form::text('firstname', null, array('class' => 'form-control', 'style' => 'margin-bottom: 10px;'))}}
            
            {{ Form::label('lasttname', 'Lastname:') }}
            {{ Form::text('lasttname', null, array('class' => 'form-control', 'style' => 'margin-bottom: 10px;'))}}
            
            {{ Form::label('password', 'Password:') }}
            {{ Form::password('password', null, array('class' => 'form-control'))}}
            
<!--              {{ Form::label('passwordConfirm', 'Confirm password:') }}
            {{ Form::password('passwordConfirm', null, array('class' => 'form-control'))}}-->
            
         
            {{Form::submit('Register', array('class' => 'btn btn-success btn-lg btn-block', 'style' => 'margin-top: 30px;'))}}
            
    
        {!! Form::close() !!}
    </div>
</div>

@endsection