@extends('main')

@section('content')
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <h1> Contact us </h1>
        <hr>
        
        {!! Form::open(array('route' => 'posts.store')) !!}
            {{ Form::label('username', 'Username:') }}
            {{ Form::text('username', null, array('class' => 'form-control', 'style' => 'margin-bottom: 10px;'))}}
            
            {{ Form::label('comment', 'Comment:') }}
            {{ Form::textarea('comment', null, array('class' => 'form-control'))}}
            
            {{Form::submit('Post comment', array('class' => 'btn btn-success btn-lg btn-block', 'style'=>'margin-top: 20px;'))}}
        {!! Form::close() !!}
    </div>
</div>
@endsection