@extends('main')

@section('content')

<!--   // Update data-->

<meta name="_token" content="{{ csrf_token() }}" />            
<div class='row'>
        {!! Form::open(['url'=>'register','id'=>'sign-up','class'=>'col-md-6 col-md-push-4 form-horizontal'])!!}
        
            <div class='form-group'>
                {!! Form::label('username', 'User Name:',['class'=>'col-xs-12 col-md-3']) !!}
                <div class= 'col-xs-12 col-md-6'>

                <input type="text" name="username" value="{{ Auth::user()->username }}" class="form-control" readonly/>
                <!-- ['class' => 'form-control']>-->
                </div>
            </div>
        
            <div class='form-group'>
                {!! Form::label('name', 'First Name:',['class'=>'col-xs-12 col-md-3']) !!}
                <div class= 'col-xs-12 col-md-6'>
                   
                     <input type="text" name="name" value="{{ Auth::user()->name }}" class="form-control" readonly />
                </div>
            </div>
            <div class='form-group'>
                {!! Form::label('lastname', 'Last Name:',['class'=>'col-xs-12 col-md-3']) !!}
                <div class= 'col-xs-12 col-md-6'>
                  
                     <input type="text" name="lastname" value="{{ Auth::user()->lastname }}" class="form-control"  readonly/>
                </div>
            </div>
            <div class='form-group'>
                {!! Form::label('email', 'Email Address:',['class'=>'col-xs-12 col-md-3']) !!}
                   <div class= 'col-xs-12 col-md-6 '>
                <input type="text" name="email" value="{{ Auth::user()->email }}" class="form-control" readonly/>
                   </div>
            </div>
             
                   
                     
<!--    <div class='form-group' style = 'margin-right: 50px; margin-top: 10px; margin-left: 100px'>
            {!! Form::label('password', 'Password:',['class'=>'col-xs-12 col-md-3', 'style' => 'margin-left: -120px;']) !!}
                <div class= 'col-xs-12 col-md-6'>
                   
                       <input type="password" name="password" value="{{ Auth::user()->password }}" class="form-control" readonly/>
                </div>
            </div>-->
    
<!--                    <div class='form-group'>
            {!! Form::label('password_confirmation', 'Confirm Password:',['class'=>'col-xs-12 col-md-3']) !!}
                <div class= 'col-xs-12 col-md-6'>
                    {!! Form::password('password_confirmation', null, ['class' => 'form-control'])!!}
                </div>
            </div>-->
                    </div>
            <div class='btn btn-small' style = 'margin-left: 350px;'>
                {!! Form::submit('Edit',['class'=>'btn btn-success btn-sm form-control', 'style' => 'margin-right: 100px;' ])!!}
            </div>
                    
        {!! Form::close() !!}
        </div>
        
        <script>
            
            $.ajaxSetup({
                
                headers:{
                      'X-CSRF-TOKEN' : $('meta[name="csft-token"]').attr('content')
                }
             });
             
             $.ajaxSetup({
                 
                 headers: {
                      'X-CSRF-TOKEN': $('[name="_token"]').val()
                 }
                     
             });
             
              $('#buttonFindUser').on('click', function () {
                  var $name = $('input[name=findUser]').val();
                  console.log($name);
                  
                  
    }
    
    $(document).ready(function () {
        
        $('usersList').css("visibility", 'hidden');
        $('username').css('visibility', 'hidden');
        $('name').css('visibility', 'hidden');
        $('name').css('visibility', 'hidden');
        $('name').css('visibility', 'hidden');
        $('name').css('visibility', 'hidden');
    })
    
    $('#alert').hide();
    
    $(#button).on('click', function() {
        
             $.ajax({
                        type:"GET",
                        url:'/code/task1/public/findUser',
                        data: {
                             'name' : $name,
                         },
                        success:function(data){
                           
                            if(data != ""){
                                $('#alert').hide();
                                $('#usersList').css("visibility", 'visible');
                                 $("#bot1").val(data[0].username);
                                 $("#bot2").val(data[0].name);
                                 $("#bot3").val(data[0].email);
                                 $("#bot4").val(data[0].firstname);
                                 $("#bot5").val(data[0].lastname);
                                // $("#bot6").val(data[0].created_at);
                                 
                            }else{
                               $('#alert').show();
                            }
                         
                        }
                        
                          success:function(data){
                           
                          $("#bot1").attr("readonly", true);
                          $("#bot1").val(data[0].username);
                         
                          $("#bot3").attr("readonly", true);
                          $("#bot3").val(data[0].email);
                          $("#bot4").attr("readonly", true);
                          $("#bot4").val(data[0].firstname);
                          $("#bot5").attr("readonly", true);
                          $("#bot5").val(data[0].lastname); 
                        }
                    });
                    
                       $('#saveUserProfile').on('click', function () {
                 
               //var $ = $('input[name=findUser]').val();  
                var $nameInput = $('input[name=userprofilename]').val();
                var $emailInput = $('input[name=userprofileemail]').val();
                var $firstNameInput = $('input[name=userprofilefirstname]').val();
                var $lastNameInput = $('input[name=userprofilelastname]').val();
                
               
                    $.ajax({
                        type:"POST",
                        url:'/code/task1/public/updateData',
                        data: {
                             'nameInput' : $nameInput,
                             'emailInput' : $emailInput,
                             'userNameInput' : $userNameInput,
                             'lastNameInput' : $lastNameInput,
                             
                         },
                        success:function(data){
                           
                          $("#bot1").attr("readonly", true);
                          $("#bot1").val(data[0].name);
                          $("#bot2").attr("readonly", true);
                          $("#bot2").val(data[0].email);
                          $("#bot3").attr("readonly", true);
                          $("#bot3").val(data[0].username);
                          $("#bot4").attr("readonly", true);
                          $("#bot4").val(data[0].lastname);
                          
                         
                         
                        }
                         
                    });
            
    });
                  
                  $(document).ready(function () {
                      
                      
                      $('#user').css('visibility', 'hidden');
                      $('#userProfile').css('visibility', 'hidden');
                      $('#frg').css('visibility', 'hidden');
                      $('#click2').on('click', function () {
                          
                          $('#alert').
                      })
                  })
             
//   $(document).ready(function () {
//           
//            $('#usersList').css("visibility", 'hidden');
//            $('#saveUserProfile').css("visibility", 'hidden');
//            /*$('#click2').on('click', function () {
//               $('#usersList').css("visibility", 'visible');
//            });*/
//       
//         $('#alert').hide();
//           
//            $('#buttonFindUser').on('click', function () {
//                  var $name = $('input[name=findUser]').val();
//                  console.log($name);
//               
//               
//         });
//           
//           $('#editUserProfile').on('click', function () {
//               $('#editUserProfile').css("visibility", 'hidden');
//               $('#saveUserProfile').css("visibility", 'visible');
//               $("#bot1").attr("readonly", false);
//               $("#bot2").attr("readonly", false);
//           });
//         
//           $('#saveUserProfile').on('click', function () {
//                 
//                var $finduser = $('input[name=findUser]').val();  
//                var $nameInput = $('input[name=userprofilename]').val();
//                //var $emailInput = $('input[name=userprofileemail]').val();
//               
//                    $.ajax({
//                        type:"POST",
//                        url:'/code/task1/public/updateUser',
//                        data: {
//                             'nameInput' : $nameInput,
//                             'finduser' : $finduser,
//                             //'emailInput' : $emailInput,
//                         },
//                        success:function(data){
//                           
//                          $("#bot1").attr("readonly", true);
//                          $("#bot1").val(data[0].name);
//                          //$("#bot2").attr("readonly", true);
//                          //$("#bot2").val(data[0].email);
//                         
//                         
//                        }
//                         
//                    });
//                    $('#editUserProfile').css("visibility", 'visible');
//                         $('#saveUserProfile').css("visibility", 'hidden');
//         });
         
 
 });
            
        </script>
            

@endsection