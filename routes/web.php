<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('main');
});

Route::get('about', function () {
    return view('about');
});

Route::get('contact', function () {
    return view('contact');
});

Route::get('login', function () {
    return view('login');
});

Route::get('/', 'PagesController@getIndex');
Route::get('contact', 'PagesController@getContact');
Route::get('about', 'PagesController@getAbout');
//Route::get('login', 'PagesController@getLogin');
//Route::get('register', 'PagesController@getRegister');
Route::get('admin', 'RolesController@store');
Route::get('edit', 'PagesController@getEdit');

Route::resource('/userprofile','userProfileController');
Route::get('/userprofile','userProfileController@create');
//Route::get('/userprofile/{id}/edit', ['as' => 'userprofile.edit', 'uses' => 'userProfileController@edit']);
Route::get('/userprofile/{id}/edit', 'userProfileController@edit');
Route::post('/userprofile/{id}', 'userProfileController@update');
Route::get('/userprofile/{id}', 'userProfileController@show');
//Route::get('/userprofile/changePassword', 'userProfileController@changePassword');
Route::get('/resetpass/{id}/index', 'ResetPasswordController@changePassword');
Route::post('/resetpass/{id}', 'ResetPasswordController@update');
Route::resource('resetpass', 'ResetPasswordController');
Route::get('search', 'PagesController@getSearch');
//Route::get('findUser', 'PagesController@getUser');

Route::match(['get','post'],'findUser', 'SearchController@findUser');
Route::match(['get','post'],'findUser', 'SearchController@findUser');
Route::get('updateData', 'PagesController@getUpdateData');

Route::resource('posts', 'PostController');

Auth::routes();

Route::get('/home', 'HomeController@index');

Route::group(['middleware' => 'web'], function () {
    Route::get('/', function () {
        return view('index');
    })->name('main');
    
    Route::get('/author', [
        'uses' => 'AppController@getAuthorPage',
        'as' => 'author',
        'middleware' => 'roles',
        'roles' => ['Admin', 'Author']
    ]);
    
    Route::get('/author/generate-article', [
        'uses' => 'AppController@getGenerateArticle',
        'as' => 'author.article',
        'middleware' => 'roles',
        'roles' => ['Author']
    ]);
    
    Route::get('/admin', [
        'uses' => 'AppController@getAdminPage',
        'as' => 'admin',
        'middleware' => 'roles',
        'roles' => ['Admin']
    ]);
    
    Route::post('/admin/assign-roles', [
        'uses' => 'AppController@postAdminAssignRoles',
        'as' => 'admin.assign',
        'middleware' => 'roles',
        'roles' => ['Admin']
    ]);
    
    });
